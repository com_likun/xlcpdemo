/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : localhost:3306
 Source Schema         : scale-server

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 13/07/2022 09:04:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pt_user
-- ----------------------------
DROP TABLE IF EXISTS `pt_user`;
CREATE TABLE `pt_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `acc_type` tinyint(4) NULL DEFAULT 1 COMMENT '账号类型：\n1. 普通账号\n2. 手机',
  `state` tinyint(4) NULL DEFAULT 1 COMMENT '账号状态：\n1. 正常\n2. 已删除',
  `opt_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `postion` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '职位描述',
  `data_access_ids` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '对于权限关联对数据id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `pt_user_account_uindex`(`account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '平台端用户（platform_user）' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pt_user
-- ----------------------------
INSERT INTO `pt_user` VALUES (1, 'sys', '$2a$10$bcGXPF6H0jbw8tWf/84D8u.DDqTDM5cD.vqslFiHnW72MHs/kyHSq', '劳资是超管', 1, 1, NULL, '2021-11-15 17:27:30', '2021-11-15 17:27:33', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
