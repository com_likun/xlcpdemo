package com.xlcp.xlcpdemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xlcp.xlcpdemo.entity.PtUser;

public interface PtUserService extends IService<PtUser> {
    /**
     * 根据账号查询
     * @param account
     * @return
     */
    PtUser findOneByAccount(String account);
}
