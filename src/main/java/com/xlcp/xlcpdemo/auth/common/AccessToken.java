package com.xlcp.xlcpdemo.auth.common;

import lombok.Data;

/**
 * @author likun
 * @date 2022年07月12日 13:43
 */
@Data
public class AccessToken {
    private String accessToken;

    private String tokenType;

    private Long expireInTimeMills;
}
