package com.xlcp.xlcpdemo.auth.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Set;

/**
 * @author likun
 * @date 2022年07月12日 12:04
 */
@Data
@ConfigurationProperties(AuthProperties.AUTH_PREFIX)
public class AuthProperties {
    public static final String AUTH_PREFIX = "config.auth.jwt";

    /**
     * token默认过期时间7天
     */
    private static final Long DEFAULT_EXPIRE_TIME = 3600000L;
    /**
     * 忽略校验的path
     */
    private Set<String> ignorePaths;

    /**
     * token过期时间(单位毫秒)
     */
    private Long expireInTimeMills = DEFAULT_EXPIRE_TIME;

    /**
     * rsa加密相关配置
     */
    private RsaProperties rsa;


    @Data
    public static class RsaProperties{
        /**
         * 加密密码
         */
        private String passWord;

        /**
         * 文件路径
         */
        private String keyPairPath;

        /**
         * 别名
         */
        private String alias;
    }


}
