package com.xlcp.xlcpdemo.auth.common;

import lombok.Data;

@Data
public class AuthenticationRequest {

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;
}
