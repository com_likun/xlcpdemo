package com.xlcp.xlcpdemo.auth.permission;

/**
 * @author likun
 * @date 2022年12月01日 14:33
 */
public interface PermissionService {
    /**
     * 判断是否拥有权限
     * @param permissions
     * @return
     */
    boolean hasPermission(String... permissions);
}
