package com.xlcp.xlcpdemo.auth.token;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.asymmetric.SignAlgorithm;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.JWTValidator;
import cn.hutool.jwt.RegisteredPayload;
import cn.hutool.jwt.signers.AlgorithmUtil;
import cn.hutool.jwt.signers.JWTSigner;
import cn.hutool.jwt.signers.JWTSignerUtil;
import com.xlcp.xlcpdemo.auth.common.AccessToken;
import com.xlcp.xlcpdemo.auth.common.AccessTokenType;
import com.xlcp.xlcpdemo.auth.common.AuthProperties;
import com.xlcp.xlcpdemo.auth.core.AccessTokenManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;

import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author likun
 * @date 2022年07月12日 13:48
 */
@Slf4j
public class JwtAccessTokenManager implements AccessTokenManager {
    private final AuthProperties authProperties;

    private final JWTSigner jwtSigner;

    public JwtAccessTokenManager(AuthProperties authProperties) throws Exception {
        this.authProperties = authProperties;
        KeyStore keyStore = KeyStore.getInstance("JKS");
        AuthProperties.RsaProperties rsaProperties = authProperties.getRsa();
        keyStore.load(ResourceUtil.getStream(rsaProperties.getKeyPairPath()),rsaProperties.getPassWord().toCharArray());
        PrivateKey privateKey = (PrivateKey) keyStore.getKey(rsaProperties.getAlias(), rsaProperties.getPassWord().toCharArray());
        PublicKey publicKey = keyStore.getCertificate(rsaProperties.getAlias()).getPublicKey();
        KeyPair keyPair = new KeyPair(publicKey, privateKey);
        this.jwtSigner=JWTSignerUtil.createSigner(AlgorithmUtil.getAlgorithm(SignAlgorithm.SHA256withRSA.getValue()),keyPair);
    }

    @Override
    public AccessToken createToken(Authentication authentication) {
        AccessToken accessToken = new AccessToken();
        accessToken.setTokenType(AccessTokenType.JWT.name());
        accessToken.setExpireInTimeMills(authProperties.getExpireInTimeMills());
        HashMap<String, Object> payloads = new HashMap<String, Object>();
        payloads.put(RegisteredPayload.AUDIENCE, authentication.getName());
        payloads.put(RegisteredPayload.JWT_ID, IdUtil.fastUUID());
        DateTime expiredAt = DateUtil.offset(new Date(), DateField.MILLISECOND, Convert.toInt(authProperties.getExpireInTimeMills()));
        payloads.put(RegisteredPayload.EXPIRES_AT, expiredAt);
        // todo 数据库查询权限信息
        List permissions = CollUtil.newArrayList("ROLE_BUYER","ROLE_SELLER","user_find_account");
        payloads.put("authDetails", permissions);
        String token = JWTUtil.createToken(payloads, this.jwtSigner);
        accessToken.setAccessToken(token);
        return accessToken;
    }

    @Override
    public Boolean verify(String accessToken) {
        JWT jwt = JWT.of(accessToken);
        jwt.setSigner(jwtSigner);
        if (!jwt.verify()){
            return Boolean.FALSE;
        }
        JWTValidator validator = JWTValidator.of(jwt);
        try {
            validator.validateAlgorithm();
            validator.validateDate();
        } catch (Exception e) {
            log.error("[jwk校验失败],失败原因是:{}",e.getMessage());
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
