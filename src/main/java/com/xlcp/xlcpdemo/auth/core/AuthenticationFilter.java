package com.xlcp.xlcpdemo.auth.core;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.RegisteredPayload;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.xlcp.xlcpdemo.auth.common.AuthProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

import static com.xlcp.xlcpdemo.entity.PtUser.ACCOUNT;

/**
 * @author likun
 * @date 2022年07月12日 15:14
 */
@Slf4j
public class AuthenticationFilter extends OncePerRequestFilter {

    private static final String BEARER = "bearer";

    private final AuthProperties authProperties;
    private final AccessTokenManager accessTokenManager;
    private final AntPathMatcher antPathMatcher;
    private final AuthenticationManager authenticationManager;

    public AuthenticationFilter(AuthProperties authProperties, AccessTokenManager accessTokenManager, AntPathMatcher antPathMatcher,AuthenticationManager authenticationManager){
        this.authProperties=authProperties;
        this.accessTokenManager=accessTokenManager;
        this.antPathMatcher=antPathMatcher;
        this.authenticationManager=authenticationManager;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // 判断当前请求是否为忽略的路径
        Set<String> ignorePaths = authProperties.getIgnorePaths();
        if (CollUtil.isNotEmpty(ignorePaths)){
            for (String ignorePath : ignorePaths) {
                if (antPathMatcher.match(ignorePath,request.getRequestURI())){
                    filterChain.doFilter(request, response);
                    return;
                }
            }
        }
        // token校验
        String bearerToken = request.getHeader(Header.AUTHORIZATION.getValue());

        if (StrUtil.isBlank(bearerToken)){
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            throw new InsufficientAuthenticationException("unauthorized request.");
        }

        final String accessToken = bearerToken.trim().substring(BEARER.length()).trim();
        boolean valid = false;
        try {
            valid = accessTokenManager.verify(accessToken);
        } catch (Exception e) {
            log.warn("verify access token [{}] failed.", accessToken);
            throw new InsufficientAuthenticationException("invalid access token + [ " + accessToken + " ].");
        }
        if (!valid) {
            throw new InsufficientAuthenticationException("invalid access token + [ " + accessToken + " ].");
        }

        final String account = request.getParameter(ACCOUNT);
        if (StringUtils.isBlank(account)) {
            SetAuthentication(accessToken);
            filterChain.doFilter(request, response);
            return;
        }
        //校验是否本人
        final String audience = JWT.of(accessToken).getPayload(RegisteredPayload.AUDIENCE).toString();
        if (!account.equalsIgnoreCase(audience)) {
            throw new AccessDeniedException("invalid account. parameter [ " + account + " ]. account in token [ " + audience + " ].");
        }
        SetAuthentication(accessToken);
        filterChain.doFilter(request, response);

    }

    private void SetAuthentication(String accessToken) {
        JwtAuthenticationToken jwtAuthenticationToken = new JwtAuthenticationToken(accessToken);
        Authentication authenticate = authenticationManager.authenticate(jwtAuthenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authenticate);
    }
}
