package com.xlcp.xlcpdemo.auth.core;

import cn.hutool.core.util.StrUtil;
import com.xlcp.xlcpdemo.auth.common.AccessToken;
import com.xlcp.xlcpdemo.auth.common.AuthenticationRequest;
import com.xlcp.xlcpdemo.common.R;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author likun
 * @date 2022年07月12日 14:57
 */
@RestController
@RequestMapping("/auth")
public class AuthenticationEndpoint {

    public static final String AUTH_ENDPOINT = "/authenticate";

    private final AuthenticationManager authenticationManager;

    private final AccessTokenManager accessTokenManager;

    public AuthenticationEndpoint(AccessTokenManager accessTokenManager, AuthenticationManager authenticationManager){
        this.authenticationManager=authenticationManager;
        this.accessTokenManager=accessTokenManager;
    }

    @PostMapping(AUTH_ENDPOINT)
    public R authentication(AuthenticationRequest authenticationRequest){
        String userName = authenticationRequest.getUserName();
        String password = authenticationRequest.getPassword();
        if (!StrUtil.isAllNotBlank(userName,password)){
            throw new BadCredentialsException("用户名或密码错误");
        }
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userName, password);
        Authentication authenticate = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        if (!authenticate.isAuthenticated()){
            throw new BadCredentialsException("user " + authenticationRequest.getUserName() + " authenticated failed.");
        }
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        final AccessToken token = accessTokenManager.createToken(authenticate);
        return R.ok(token);
    }


}
