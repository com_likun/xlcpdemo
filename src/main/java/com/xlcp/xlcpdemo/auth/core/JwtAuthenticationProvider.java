package com.xlcp.xlcpdemo.auth.core;

import cn.hutool.jwt.JWT;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.List;

/**
 * @author likun
 * @date 2022年12月01日 12:25
 */
public class JwtAuthenticationProvider implements AuthenticationProvider {
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
        String token = jwtAuthenticationToken.getToken();
        JWT jwt = JWT.create().parse(token);
        Object authDetails = jwt.getPayload("authDetails");
        Object aud = jwt.getPayload("aud");
        List<GrantedAuthority> permissions;
        if (authDetails!=null&&authDetails instanceof List){
            List<String> auths = (List<String>) authDetails;
            permissions=AuthorityUtils.createAuthorityList(auths.toArray(new String[0]));
        }else {
            permissions = AuthorityUtils.createAuthorityList("");
        }
        return new JwtAuthenticationToken(aud,null,permissions);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
