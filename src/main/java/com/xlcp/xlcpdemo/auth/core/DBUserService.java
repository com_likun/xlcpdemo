package com.xlcp.xlcpdemo.auth.core;

import com.xlcp.xlcpdemo.entity.PtUser;
import com.xlcp.xlcpdemo.service.PtUserService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;

public class DBUserService implements UserDetailsService {

    private PtUserService ptUserService;

    public DBUserService(PtUserService ptUserService) {
        this.ptUserService = ptUserService;
    }

    @Override
    public UserDetails loadUserByUsername(String account) throws UsernameNotFoundException {

        final PtUser ptUser = ptUserService.findOneByAccount(account);

        if (ptUser == null) {
            throw new UsernameNotFoundException("account [" + account + " ] not found.");
        }

        return new User(ptUser.getAccount(), ptUser.getPassword(),true, true, true, true, new ArrayList<>());
    }
}
