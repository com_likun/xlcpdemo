package com.xlcp.xlcpdemo.auth.core;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.exceptions.ExceptionUtil;
import com.xlcp.xlcpdemo.common.CommonConstants;
import com.xlcp.xlcpdemo.common.R;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
@RequestMapping("${server.error.path:${error.path:/error}}")
public class GlobalExceptionController extends BasicErrorController {
    private final ErrorAttributes errorAttributes;

    public GlobalExceptionController(ErrorAttributes errorAttributes, ErrorProperties errorProperties) {
        super(errorAttributes, errorProperties);
        this.errorAttributes = errorAttributes;

    }

    @Override
    @RequestMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        HttpStatus status = this.getStatus(request);
        if (status == HttpStatus.NO_CONTENT) {
            return new ResponseEntity(status);
        } else {
            ServletWebRequest servletWebRequest = new ServletWebRequest(request);
            Throwable error = errorAttributes.getError(servletWebRequest);
            if (error!=null){
                error = ExceptionUtil.getRootCause(error);
                if (error instanceof AuthenticationException){
                    R result = new R();
                    result.setMsg("用户凭证已过期");
                    result.setCode(CommonConstants.FAIL);
                    Map<String, Object> body = BeanUtil.beanToMap(result);
                    return new ResponseEntity(body, HttpStatus.UNAUTHORIZED);
                }
            }
            Map<String, Object> body = this.getErrorAttributes(request, this.getErrorAttributeOptions(request, MediaType.ALL));
            return new ResponseEntity(body, status);
        }
    }
}
