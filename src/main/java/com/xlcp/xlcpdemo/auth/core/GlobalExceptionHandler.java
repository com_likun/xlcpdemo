package com.xlcp.xlcpdemo.auth.core;

import com.xlcp.xlcpdemo.common.R;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler({BadCredentialsException.class})
    public ResponseEntity<R> handleBadCredentialsException(BadCredentialsException e) {
        return new ResponseEntity(R.failed("用户名或密码错误"), HttpStatus.BAD_REQUEST);
    }

}
