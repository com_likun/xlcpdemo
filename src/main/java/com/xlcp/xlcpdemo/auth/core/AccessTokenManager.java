package com.xlcp.xlcpdemo.auth.core;

import com.xlcp.xlcpdemo.auth.common.AccessToken;
import org.springframework.security.core.Authentication;


/**
 * @author likun
 * @date 2022年07月12日 13:42
 */
public interface AccessTokenManager {
    /**
     * 创造token
     * @param authentication
     * @return
     */
    AccessToken createToken(Authentication authentication);

    /**
     * 校验token是否有效
     * @param accessToken
     * @return
     */
    Boolean verify(String accessToken);
}
