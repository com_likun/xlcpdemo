package com.xlcp.xlcpdemo.auth.core;

import lombok.Getter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author likun
 * @date 2022年12月01日 11:51
 */
public class JwtAuthenticationToken extends AbstractAuthenticationToken {
    private  Object principal;

    private Object credentials;

    @Getter
    private String token;

    public JwtAuthenticationToken(Object principal,Object credentials,Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal= principal;
        this.credentials= credentials;
        setAuthenticated(true);
    }

    public JwtAuthenticationToken(String token){
        super(null);
        this.token=token;
        setAuthenticated(false);
    }

    @Override
    public Object getCredentials() {
        return credentials;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }


}
