package com.xlcp.xlcpdemo.auth.exception;

/**
 * @author likun
 * @date 2022年07月12日 17:40
 */
public class AuthException extends RuntimeException{
    public AuthException(String msg){
        super(msg);
    }
}
