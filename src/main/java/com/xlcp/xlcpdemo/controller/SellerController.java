package com.xlcp.xlcpdemo.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author likun
 */
@RestController
@RequestMapping("/seller")
public class SellerController {

    /**
     * 卖家接单
     *
     * @return
     */
    @GetMapping("/order:receive")
	@Secured("ROLE_SELLER")
    public String receiveOrder() {
        return "卖家接单啦！";
    }

    /**
     * 卖家订单发货
     *
     * @return
     */
    @GetMapping("/order:deliver")
    @Secured("ROLE_SELLER")
    public String deliverOrder() {
        return "卖家发货啦！";
    }
}
