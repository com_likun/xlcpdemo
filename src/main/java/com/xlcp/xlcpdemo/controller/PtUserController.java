package com.xlcp.xlcpdemo.controller;

import cn.hutool.core.util.StrUtil;
import com.xlcp.xlcpdemo.auth.common.AccessToken;
import com.xlcp.xlcpdemo.auth.common.AuthenticationRequest;
import com.xlcp.xlcpdemo.auth.core.AccessTokenManager;
import com.xlcp.xlcpdemo.common.R;
import com.xlcp.xlcpdemo.entity.PtUser;
import com.xlcp.xlcpdemo.service.PtUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@Slf4j
public class PtUserController {
    @Autowired
    private PtUserService ptUserService;
    @Autowired
    private AccessTokenManager accessTokenManager;

    @Autowired
    private AuthenticationManager authenticationManager;


    @GetMapping("/findOneByAccount")
    @PreAuthorize("@pms.hasPermission('user_find_account')")
    PtUser findOneByAccount(String account){
        return ptUserService.findOneByAccount(account);
    }

    @PostMapping("/testToken")
    public R testToken(AuthenticationRequest request){
        String userName = request.getUserName();
        String password = request.getPassword();
        if (!StrUtil.isAllNotBlank(userName,password)){
            return R.failed("用户名或密码不能为空!");
        }
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userName, password);
        Authentication authenticate = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        if (!authenticate.isAuthenticated()){
            throw new BadCredentialsException("user " + request.getUserName() + " authenticated failed.");
        }
        final AccessToken token = accessTokenManager.createToken(authenticate);
        return R.ok(token);
    }


}
