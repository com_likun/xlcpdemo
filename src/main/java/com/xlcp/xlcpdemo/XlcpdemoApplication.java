package com.xlcp.xlcpdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xlcp.xlcpdemo.mapper")
public class XlcpdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(XlcpdemoApplication.class, args);
    }

}
