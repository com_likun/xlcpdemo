package com.xlcp.xlcpdemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PtUser {

    private static final long serialVersionUID = 1L;


    @TableId(
            value = "id",
            type = IdType.AUTO
    )
    private Integer id;


    private String account;


    private String password;


    private String name;


    private Integer accType;


    private Integer state;


    private String optUser;


    private String postion;


    private String dataAccessIds;

    private LocalDateTime createAt;

    private LocalDateTime updateAt;


    public static final String ACCOUNT = "account";

    public static final String PASSWORD = "password";

    public static final String NAME = "name";

    public static final String ACC_TYPE = "acc_type";

    public static final String STATE = "state";

    public static final String OPT_USER = "opt_user";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

}
