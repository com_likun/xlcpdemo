package com.xlcp.xlcpdemo.mapper.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xlcp.xlcpdemo.entity.PtUser;
import com.xlcp.xlcpdemo.mapper.PtUserMapper;
import com.xlcp.xlcpdemo.service.PtUserService;
import org.springframework.stereotype.Service;

@Service
public class PtUserServiceImpl extends ServiceImpl<PtUserMapper, PtUser> implements PtUserService {

    @Override
    public PtUser findOneByAccount(String account) {
        QueryWrapper<PtUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(PtUser.ACCOUNT, account);
        return getOne(queryWrapper);
    }
}
