package com.xlcp.xlcpdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xlcp.xlcpdemo.entity.PtUser;

public interface PtUserMapper extends BaseMapper<PtUser> {

}
